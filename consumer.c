#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include<string.h>
#include<errno.h>
#include<time.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h> 

#define PRODUCER 0  
#define CONSUMER 1  

key_t klucz;
int semid;
int segment;
int odlacz_adres;
int odlacz_segment;
char* adres;
FILE* out_file;

static void utworz_klucz();
void utworz_nowy_segment(key_t);
void dolacz_pamiec();
void odlacz_pamiec();
static void utworz_nowy_semafor(key_t);
static void semafor_p(int semnum);
static void semafor_v(int semnum);
static void usun_semafor();


int main(int argc, char* argv[])
{
    if (argc != 2) // liczba arg =2, dla ./consumer oraz wyjsciowy.txt 
    {
        perror("Blad konsument. Zla liczba argumentow. Nie podano pliku wejsciowego\n");
    }
    else
    {
        out_file = fopen(argv[1], "w+");
        if (out_file == NULL)
        {
            perror("Nie otworzono pliku\n");
            odlacz_pamiec();
            usun_semafor();
            printf("KONSUMENT: koniec programu\n");
            exit(EXIT_FAILURE);
        }
        else
        {
            utworz_klucz();
            utworz_nowy_segment(klucz);
            dolacz_pamiec();
            utworz_nowy_semafor(klucz);
            printf("Sprawdzam: utworzono zbior semaforow %d:\n", semid);

            char a;
            do{
                sleep(rand() % 5);
                /********************* sekcja krytyczna ***********************/
                semafor_p(CONSUMER);
                a = *adres; //tutaj a=EOF i wyjscie z while
                if ( a != EOF) 
                {
                    fputc(a, out_file);
                    printf("Skonsumowano:\t %c\n", a);
                  
                    semafor_v(PRODUCER);
                }
            } while (a != EOF);
            
            if (fclose(out_file) != 0)
                perror("Blad zamkniecia pliku wyjsciowego\n");
            out_file = NULL;
            odlacz_pamiec();
            usun_semafor();
        }
    }
    printf("KONSUMENT: koniec programu\n");
    exit(0);

};


static void utworz_klucz() {

    //if ((klucz = ftok(".", 'E')) == -1)
    if ((klucz =4321) == -1)
    {
        perror("Konsumer: Blad otwarcia pliku do zapisu\n");
        exit(EXIT_FAILURE);
    }
    else
        printf("\nZostal utworzony klucz: %d\n", klucz);
}

void utworz_nowy_segment(key_t klucz)
{
    if ((segment = shmget(klucz, sizeof(char) , 0660 | IPC_CREAT)) == -1) // zwraca id zbioru semaforow, jesli istnial zwroci ten sam, id
    {
        perror("Konsument: Problemy z utworzeniem pamieci dzielonej.\n");
        exit(EXIT_FAILURE);
    }
    else
    {
        printf("Konsument: Pamiec dzielona ID: %d utworzona.\n", segment);
    }
};

void dolacz_pamiec()
{
    adres = (char*)shmat(segment, 0, 0);
};

void odlacz_pamiec()
{
    odlacz_adres = shmctl(segment, IPC_RMID, 0);
    odlacz_segment = shmdt(adres);
    if (odlacz_adres == -1 || odlacz_segment == -1)
    {
        perror("Konsument: Problemy z odlaczeniem pamieci dzielonej.\n");
        exit(EXIT_FAILURE);
    }
    else printf("Konsument: Pamiec dzielona zostala odlaczona.\n");
};

static void utworz_nowy_semafor(key_t klucz)
{
    semid = semget(klucz, 2, 0660 | IPC_CREAT); // zwraca id zbioru semaforow, jesli istnial zwroci ten sam, id
    if (semid == -1)
    {
        perror("Nie moglem utworzyc nowego semafora.\n");
        exit(EXIT_FAILURE);
    }
    else
    {
        printf("Konsument: Otrzymano semafor: %d z kluczem: %d\n", semid, klucz);
    }
};

static void usun_semafor()
{
    if (semctl(semid, 0, IPC_RMID) == -1) //semctl - Sterowanie semaforami; IPC_RMID � usuni�cie danego zbioru semafor�w. 
    {
        perror("Nie mozna usunac semafora.\n");
        exit(EXIT_FAILURE);
    }
    else
    {
        printf("Usunieto zbior semaforow: %d\n\n", semid);
    }
};

static void semafor_p(int semnum)
{
    int zmien_sem;
    struct sembuf bufor_sem;
    bufor_sem.sem_num = semnum;
    bufor_sem.sem_op = -1; // obnizenie wartosc
    bufor_sem.sem_flg = 0;

    while (1) {
        zmien_sem = semop(semid, &bufor_sem, 1); //semop -Operacje na semaforach ;

        if (zmien_sem == 0 || !(errno == 4))
            break;
    }

    if (zmien_sem == -1)
    {
        if (errno != 4)
        {
            semid = semget(klucz, 2, 0660 | IPC_CREAT);
            if (semid == -1) {
                perror("Blad w semget dla dostepu do semafora\n");
                printf("Nie uzyskano dostepu do semafora.\n");
                exit(EXIT_FAILURE);
            }
            else {
                printf("Mam dostep do zbioru semaforow : %d\n", semid);
            }
        }
    }
    else
    {
        printf("Semafor zostal zamkniety.\n\n");
    }
};

static void semafor_v(int semnum)
{
    int zmien_sem;
    struct sembuf bufor_sem;
    bufor_sem.sem_num = semnum;
    bufor_sem.sem_op = 1; // operacja podniesienie semafora bo mamy 1
    bufor_sem.sem_flg = 0;// flaga pozwala na cofniecie operacji

    while (1) {
        zmien_sem = semop(semid, &bufor_sem, 1);

        if (zmien_sem == 0 || !(errno == 4))
            break;
    }

    if (zmien_sem == -1)
    {
        if (errno != 4)
        {
            semid = semget(klucz, 2, 0660 | IPC_CREAT);
            if (semid == -1) {
                perror("Blad w semget dla dostepu do semafora\n");
                printf("Nie uzyskano dostepu do semafora.\n");
                exit(EXIT_FAILURE);
            }
            else {
                printf("Mam dostep do zbioru semaforow : %d\n", semid);
            }
        }
    }
    else
    {
        printf("Semafor zostal otwarty.\n\n");
    }
};
